#!/usr/bin/env python3

#Motion Lamp 1.0
#author : Alessio Trivisonno
#email : alessio.trivisonno@yahoo.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import RPi.GPIO as GPIO
import time
import datetime
import signal
import sys
import os

GPIO.setmode(GPIO.BCM)
pin_ir = 6
pin_light_s = 13
led_stat = 5
# button_stat = 12
# GPIO.setup(button_stat, GPIO.IN)
GPIO.setup(pin_light_s, GPIO.IN)
GPIO.setup(pin_ir, GPIO.IN)

wfPath = "/home/fox/test-python/relay/lamp/p1"

#toggle the light switch
def toggle(pin_ir):

    if (on == True):
#the following if is needed to prevent casual activation of the light due to electric instability i.e. if you turn on a device it may trigger
#the interrupt detection
        if (GPIO.input(pin_ir) == GPIO.HIGH):

#calculate the schedule time
            nowh = datetime.datetime.now()
            curr_hour = int(nowh.hour)
            # print (curr_hour)
            if (curr_hour >= 23 or curr_hour <= 8):
                # print ("out of schedule")
                return
            print ("motion detected\n")
            now = time.time()
            # light = 31#debug
            light = RCtime(pin_light_s)
            # print (light)
#this gives the difference between this and the previous triggered time
            delta_time = now - prev_trigg
            # print (delta_time)
            if (delta_time > 10):
                delay_reset()
                # print ("delay now reset to", delay)
            if (light > 45):

#exponential backoff in case on fast retrigger
                if ( delta_time <= 10 ):
                    delay_increase(delay)
                    # print ("delay now is", delay)
                turnon()
#lcd running indicator
                display_notify(delay)
                time.sleep(delay)
                turnoff()
#update the previus time
            prev_set(time.time())

def display_notify(tmp):
    p = open("/home/fox/test-python/lcd/.p1", "w")
    timer = "LAMP" + str(tmp)
    p.write(timer)
    p.close()

#resets the global variable delay to the default value
def delay_reset():
    global delay
    delay = 90

#increase the delay of a certain multiplier (simil-exponential backoff)
def delay_increase(val):
    global delay
    delay = val * 1.7

#sets the previous trigger time to val
def prev_set(val):
    global prev_trigg
    prev_trigg = val


#turn on the lamp
def turnon ():
    wp = open(wfPath, 'w')
    wp.write("1")        
    wp.close()

#turn off the lamp
def turnoff ():
    wp = open(wfPath, 'w')
    wp.write("0")        
    wp.close()

#handle polite closing by SIGTERM and SIGINT (ctrl-c)
def handler (signal, frame):
    print ("Closing... Bye!!")
    wp = open(wfPath, 'w')
    wp.write("0")        
    wp.close()
    GPIO.cleanup()
    sys.exit(0)

#read the value of the brightness from the sensor
def RCtime (RCpin):
    reading = 0
    GPIO.setup(RCpin, GPIO.OUT)
    GPIO.output(RCpin, GPIO.LOW)
    time.sleep(0.1)

    GPIO.setup(RCpin, GPIO.IN)

# This takes about 1 millisecond per loop cycle
    while (GPIO.input(RCpin) == GPIO.LOW and reading < 100):
        reading += 1
        time.sleep(0.01)
    return reading


#wether the motion lamp is on or off
on = True
prev_trigg = 0
delay = 90

# def panel (pin):
    # time.sleep(3)
    # if (GPIO.input(pin) == GPIO.HIGH):
        # if (on == True):
            # n = 0
            # while ( n < 5):
                # GPIO.output (led_stat, GPIO.HIGH)
                # time.sleep(0.5)
                # GPIO.output (led_stat, GPIO.LOW)
                # time.sleep(0.5)
                # n = n + 1
            # disable()
            # print("disabling")
        # else:
            # enable()
            # print("enabling")

    # GPIO.output(led_stat, GPIO.HIGH)

# def enable():
    # global on
    # on = True
# def disable():
    # global on
    # on = False
    # turnoff()

# GPIO.add_event_detect(button_stat, GPIO.RISING, callback=panel, bouncetime=1500)
GPIO.add_event_detect(pin_ir, GPIO.RISING, callback=toggle, bouncetime=1500)

signal.signal (signal.SIGTERM, handler)
signal.signal (signal.SIGINT, handler)

#wait for a signal to occur
signal.pause()

